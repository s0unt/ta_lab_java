package com.epam.lab.loggers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SomeLogic {
    private static final Logger log = LogManager.getLogger(SomeLogic.class);

    public void doOrder(){
        // some logic
        System.out.println("Ordered!");
        log.info("This is info message");
        addToCart();
    }

    private void addToCart() {
        // add item to cart
        System.out.println("Item added");
        log.error("This is error message!");
    }
}
