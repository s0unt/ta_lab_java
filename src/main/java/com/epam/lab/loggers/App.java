package com.epam.lab.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    private static Logger logger = LogManager.getLogger(SomeLogic.class);
    private static SomeLogic logic;


    public static void main(String[] args) {
        logic = new SomeLogic();
        logic.doOrder();
    }

}