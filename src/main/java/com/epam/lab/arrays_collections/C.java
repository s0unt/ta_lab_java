package com.epam.lab.arrays_collections;

public class C {
    public static int[] removeDuplicates ( int[] numbersWithDuplicates){
        int[] result = new int[numbersWithDuplicates.length];
        int previous = numbersWithDuplicates[0];
        result[0] = previous;
        for (int i = 1; i < numbersWithDuplicates.length; i++) {
            int ch = numbersWithDuplicates[i];
            if (previous != ch) {
                result[i] = ch;
            }
            previous = ch;
        }
        int notZeros = result.length - countZeros(result);
        int[] finalArr = new int[notZeros];
        int position = 0;
        for (int i = 0; i < result.length; i++) {
            if (result[i] != 0) {
                finalArr[position] = result[i];
                position++;
            }
        }
        return finalArr;
    }
    public static int countZeros ( int[] arr){
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                counter++;
            }
        }
        return counter;
    }
}
