package com.epam.lab.arrays_collections;

import java.util.HashSet;
import java.util.Set;

public class A {
    public static int[] newArrayFromTwoArrays(int[] array1, int[] array2) {
        int[] result = new int[array1.length + array2.length];
        Set<Integer> set = new HashSet<>();
        int pos = 0;
        for (int element : array1) {
            result[pos++] = element;
        }
        for (int element : array2) {
            result[pos++] = element;
        }
        for (int i : result) set.add(i);

        int[] e = new int[set.size()];
        pos = 0;
        for (Object object : set) {
            e[pos++] = (int) object;
        }
        return e;
    }

    public static int[] newArrayFromExisted(int[] array) {
        int[] result = new int[array.length];
        Set<Integer> set = new HashSet<>();
        int pos = 0;
        for (int element : array) {
            result[pos++] = element;
        }
        for (int i : result)
            set.add(i);
        int[] e = new int[set.size()];
        pos = 0;
        for (Object object : set) {
            e[pos++] = (int) object;
        }
        return e;
    }
}
