package com.epam.lab.arrays_collections;

import java.util.HashMap;
import java.util.Map;

public class B {
    public static void removeTripleElements(int array[]) {
        int n = array.length;
        Map<Integer, Integer> mp = new HashMap<>();

        for (int i = 0; i < n; ++i)
            mp.put(array[i], mp.get(array[i]) == null ? 1 : mp.get(array[i]) + 1);


        for (int i = 0; i < n; ++i)
            // Print the element which appear
            // less than or equal to k times.
            if (mp.containsKey(array[i]) && mp.get(array[i]) < 3)
                System.out.print(array[i] + " ");
    }
}
