package com.epam.lab.arrays_collections;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private Random random = new Random();
    private static int power = 25;
    private int deathDoors;
    private final int DOORS_COUNT = 10;

    private int[] createDoors() {
        int[] doors = new int[DOORS_COUNT];
        for (int i = 0; i < doors.length; i++) {
            if (isMonster())
                doors[i] = -1 * (5 + random.nextInt(96));
            else
                doors[i] = 10 + random.nextInt(71);
        }
        return doors;
    }

    private boolean isMonster() {
        Random random = new Random();
        return random.nextBoolean();
    }

    private void printDoors(int[] doors) {
        for (int i : doors) System.out.println(i);
    }

    private void openDoor(int[] doors, int power) {
        int door = 44;
        boolean ready;
        int[] selected = new int[DOORS_COUNT];
        for (int i=0; i<10; i++) selected[i] = 900;
        System.out.println("Hello! You have 25 health score, please open door from 1 to 10: ");
        for (int i = 0; i < DOORS_COUNT; i++) {
            ready = false;
            while (!ready) {
                try {
                    Scanner scanner = new Scanner(System.in);
                    door = scanner.nextInt() - 1;
                    if (contains(selected, door)) throw new IllegalArgumentException();
                        else ready = true;
                    if (door < 0 || door > 10) {
                        System.out.println("Oops, the number you've entered is out of range!");
                        ready = false;
                    } else ready = true;
                } catch (InputMismatchException e) {
                    System.out.println("The value you've entered is non-numeric.");
                } catch (IllegalArgumentException a) {
                    System.out.println("This door is already opened!");
                }
            }


            if (doors[door] < 0) {
                power += doors[door];
                if (power < 0) {
                    System.out.println("There was an enemy in the " + (door+1) + " door with " + doors[door] + " health and you could not beat him.");
                    break;
                }
                System.out.println("There was an enemy in the " + (door+1) + " door with " + doors[door] + " power, but you've beated him, " +
                        "now you have " + power + " power");
            } else {
                power += doors[door];
                System.out.println("There was heal in the " + (door+1) + " with " + doors[door] + " health" +
                        " now you have " + power + " power");

            }
            selected[i] = door;
        }
    }


    public static void main(String[] args) {
        Game game = new Game();
        int[] doors = game.createDoors();
        game.printDoors(doors);
        game.openDoor(doors, power);

    }

    public static boolean contains(final int[] array, final int v) {
        boolean result = false;
        for (int i : array)
            if (i == v) {
                result = true;
                break;
            }
        return result;
    }
}

