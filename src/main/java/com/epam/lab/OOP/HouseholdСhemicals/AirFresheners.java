package com.epam.lab.OOP.HouseholdСhemicals;



public class AirFresheners extends Good {
    enum Type {
        Spray, Aerosol, Automatic, Air_Freshener, Replaceable_Balloon_for_automatic_Air_Freshener, Aromablock, Odor_neutralizer
    }
    Type a;
    public AirFresheners(int id, String name, Brand brand, Country country, double price, Type type) {
        super(id, name, brand, country, price);
        this.a = type;
    }
    public Type getType() { return a; }
}
