package com.epam.lab.OOP.HouseholdСhemicals;

public class Brand {
    private String name;

    public Brand(String name) { this.name = name; }
    public String getName() { return name; }

    @Override
    public String toString() {
        return "Brand: " + name;
    }
}
