package com.epam.lab.OOP.HouseholdСhemicals;


public class CleaningProducts extends Good {
    enum Type {
        KitchenMeans, Bathroom_Facilities, Floor_Cleaners, Toiletries,Universal_Detergents, Window_Cleaners, Carpet_Cleaners,
        Pipe_Cleaning, Furniture_Care_Means, Cleaning_Equipment
    }
    Type a;
    public CleaningProducts(int id, String name, Brand brand, Country country, double price, Type type) {
        super(id, name, brand, country, price);
        this.a = type;
    }
    public Type getType() { return a; }
}
