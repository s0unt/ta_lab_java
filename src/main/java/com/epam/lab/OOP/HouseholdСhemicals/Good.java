package com.epam.lab.OOP.HouseholdСhemicals;


import java.util.ArrayList;
import java.util.Arrays;

public class Good {
    private int id;
    private String name;
    private Brand brand;
    private Country country;
    private double price;
    public ArrayList<Good> Sklad = new ArrayList<Good>();

    public enum Type{
        SPECIALIZED, LAUNDRY_CONDITIONER, ANTI_INSECT_COSMETIC, ANTI_INSECT_CHEMICALS, CLEANING_WIPES, HOUSEHOLD_SPONGES, CLOTHING_CARE, LAUNDRY_ACCESSORIES
    }
    Type a;
    public Good(int id, String name, Brand brand, Country country, double price, Type type){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.country = country;
        this.price = price;
        this.a = type;
    }

    public Good(int id, String name, Brand brand, Country country, double price){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.country = country;
        this.price = price;
    }

    public int getId() { return id; }
    public void setId() { this.id = id; }
    public String getName() { return name; }
    public void setName() { this.name = name; }
    public Brand getBrand() { return brand; }
    public void setBrand() { this.brand = brand; }
    public Country getCountry() { return country; }
    public void setCountry() { this.country = country; }
    public double getPrice() { return price; }
    public void setPrice() { this.price = price; }
    protected Type getGoodType() {return a;}

    public void addGood(Good f){ Sklad.add(f); }
    public void removeGood(Good f){ Sklad.remove(f); }
    public void printSklad(){ System.out.println(Arrays.toString(Sklad.toArray())); }
}
