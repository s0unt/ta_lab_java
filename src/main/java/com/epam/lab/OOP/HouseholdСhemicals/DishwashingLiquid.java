package com.epam.lab.OOP.HouseholdСhemicals;



public class DishwashingLiquid extends Good {
    enum Type {
        Dishwashing_liquid, Organic, ForDishesInMachine, Tablets, Powder, Salt, Rinse, Purifier
    }
    Type a;
    public DishwashingLiquid(int id, String name, Brand brand, Country country, double price,Type type) {
        super(id, name, brand, country, price);
        this.a = type;
    }
    public Type getType() { return a; }
}
