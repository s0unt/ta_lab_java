package com.epam.lab.OOP.HouseholdСhemicals;

public class WashingPowder extends Good {
    enum Type {
        LOOSE,
        LIQUID,
        GEL,
        CAPSULES,
        HAND_WASHING,
        CHILDERNS_UNDERWEAR,
        WOOL_AND_SILK,
        ORGANIC,
        HYPOALLERGENIC

    }

    Type a;
    public WashingPowder(int id, String name, Brand brand, Country country, double price, Type type) {
        super(id, name, brand, country, price);
        this.a=type;
    }

    public Type getType() { return a; }
}




