package com.epam.lab.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("You entered non-numeric value.");
            }
    }

    public static int squareRectangle(int a, int b) throws Exception {
        int k = a * b;
        if (a <= 0 || b <= 0)
            throw new IllegalArgumentException("a or b is below 0");
        return k;
    }
}
