package com.epam.lab.game;

import java.util.Scanner;

public class Character {
    Scanner sc = new Scanner(System.in);
    public String name;
    public int health;
    public enum CharacterClass {
        ASSASSIN("Assasin"), WARRIOR("Warrior"), WIZARD("Wizzard");
        String classString;
        private CharacterClass (String classString) {
            this.classString = classString;
        }
        public String character() {
            return classString;
        }
    }
    CharacterClass a;
    public Character(String name, int health, CharacterClass CharacterClass){
        this.name = name;
        this.health = health;
        a = CharacterClass;
    }
    public Character(){

    }
    public void setName() {
        this.name = sc.nextLine();
    }
    public CharacterClass getCharacterClass() {
        return a;
    }
    public void heal(int weed){
        if (this.health>=200)
            return;
        else
            this.health+=30;
    }
    @Override
    public String toString() {
        return a.character();
    }

}
