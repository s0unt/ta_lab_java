package com.epam.lab.game;
import java.util.Random;

public class Enemy {
    public int health;
    public enum EnemyType {
        SKELETON("Skeleton"), ZOMBIE("Zombie"), DESERTER("Deserter");
        String enemy;
        private EnemyType (String enemy) {
            this.enemy = enemy;
        }
        public String enemy() {
            return enemy;
        }
    }
    EnemyType a;
    public Enemy(int health, EnemyType enemyType){
        this.health = health;
        a = enemyType;
    }
    public void generateDamage(){
        Random random = new Random();
        this.health -= random.nextInt(30);
    }
    @Override
    public String toString() {
        return a.enemy();
    }
}
