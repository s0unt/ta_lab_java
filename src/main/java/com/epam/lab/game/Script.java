package com.epam.lab.game;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Script {
    public void startGame() {
        System.out.println("Welcome to the new game, Please select your character");
        int select = 3;
        boolean ready = false;
        while (!ready) {
            System.out.println("Select your character's Class: \n1. Assassin \n2. Warrior\n3. Wizzard");
            try {
                Scanner scanner = new Scanner(System.in);
                select = scanner.nextInt();
                if (select < 1 || select > 3) {
                    System.out.println("Oops, the number you've entered is out of range!");
                    ready = false;
                } else ready = true;
            } catch (InputMismatchException e) {
                System.out.println("The value you've entered is non-numeric.");
            }
        }
        Character character = new Character();
        if (select == 1) {
            character = new Character("s", 100, Character.CharacterClass.ASSASSIN);
            System.out.println("You have chosen class Assassin, enter your character's name: ");
            character.setName();
            System.out.println("Hello " + character.name + " " + character.toString());
        } else if (select == 2) {
            character = new Character("s", 100, Character.CharacterClass.WARRIOR);
            System.out.println("You have chosen class Warrior, enter your character's name: ");
            character.setName();
            System.out.println("Hello " + character.name + " " + character.toString());
        } else {
            character = new Character("s", 100, Character.CharacterClass.WIZARD);
            System.out.println("You have chosen class Wizard, enter your character's name: ");
            character.setName();
            System.out.println("Hello " + character.name + " " + character.toString());
        }

    }
}
